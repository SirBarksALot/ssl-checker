package main

import (
	"crypto/tls"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net"
	"os"
	"strconv"
	"sync"
	"time"
)

type SafeArray struct {
	a   []Result
	mux sync.Mutex
}

type Domains struct {
	Domains []string `json:"domains"`
}

type Result struct {
	Name   string
	Valid  bool
	Reason string
	Time   string
}

func (results *SafeArray) dataAppend(data Result) {
	results.mux.Lock()
	// Lock so only one goroutine at a time can access the array
	results.a = append(results.a, data)
	results.mux.Unlock()
}

func checkDomainCertificate(domain string) Result {
	var host = domain + ":443"

	t, _ := strconv.Atoi(os.Getenv("MAX_CHECK_TIME"))
	dialer := &net.Dialer{
		Timeout: time.Second * time.Duration(t),
	}
	conn, err := tls.DialWithDialer(dialer, "tcp", host, &tls.Config{})
	if err != nil {
		return Result{Name: domain, Valid: false, Reason: err.Error()}
	}
	defer func(conn *tls.Conn) {
		err := conn.Close()
		if err != nil {
		}
	}(conn)

	cert := conn.ConnectionState().PeerCertificates[0]
	timeNow := time.Now()
	expiresIn := strconv.FormatFloat(cert.NotAfter.Sub(timeNow).Hours()/24, 'f', 0, 64)

	return Result{Name: domain, Valid: true, Reason: expiresIn + " days to expiry"}
}

func checkCerts(c *gin.Context) {
	var domains Domains
	var results SafeArray

	err := c.BindJSON(&domains)
	if err != nil {
		return
	}

	concurrent, _ := strconv.Atoi(os.Getenv("CONCURRENCY"))

	var ch = make(chan string)
	var wg sync.WaitGroup

	wg.Add(concurrent)
	for i := 0; i < concurrent; i++ {
		go func() {
			for {
				domain, ok := <-ch
				if !ok { // if there is nothing to do and the channel has been closed then end the goroutine
					wg.Done()
					return
				}
				start := time.Now()
				result := checkDomainCertificate(domain)
				elapsed := time.Since(start)
				result.Time = elapsed.String()
				results.dataAppend(result)
			}
		}()
	}

	for _, domain := range domains.Domains {
		ch <- domain // add domain to the queue
	}

	close(ch) // This tells the goroutines there's nothing else to do
	wg.Wait() // Wait for the goroutines to finish

	prettyPrint(results.a)

	c.JSON(200, gin.H{
		"message": results.a,
	})
}

func prettyPrint(v interface{}) (err error) {
	b, err := json.MarshalIndent(v, "", "  ")
	if err == nil {
		log.Println(string(b))
	}
	return
}

func main() {
	r := gin.Default()
	r.POST("/ssl-check", checkCerts)
	err := r.Run()
	if err != nil {
		return
	}
}
