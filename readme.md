# ssl-check

## Run

```bash
docker compose up --build
```

## Sample usage

```bash
curl --location --request POST 'http://127.0.0.1:8000/ssl-check' \
--header 'Content-Type: application/json' \
--data-raw '{
    "domains": [
        "google.com",
        "test-ev-rsa.ssl.com",
        "expired-rsa-dv.ssl.com",
        "revoked-rsa-dv.ssl.com",
        "domain-u-cannot-connect-2.com"
    ]
}'
```

## Sample output

```bash
{
    "message": [
        {
            "Name": "domain-u-cannot-connect-2.com",
            "Valid": false,
            "Reason": "dial tcp: lookup domain-u-cannot-connect-2.com on 127.0.0.11:53: no such host",
            "Time": "81.0416ms"
        },
        {
            "Name": "google.com",
            "Valid": true,
            "Reason": "49 days to expiry",
            "Time": "182.1569ms"
        },
        {
            "Name": "expired-rsa-dv.ssl.com",
            "Valid": false,
            "Reason": "x509: certificate has expired or is not yet valid: current time 2021-08-01T20:46:12Z is after 2016-08-02T20:48:30Z",
            "Time": "581.5455ms"
        },
        {
            "Name": "test-ev-rsa.ssl.com",
            "Valid": true,
            "Reason": "363 days to expiry",
            "Time": "681.6515ms"
        },
        {
            "Name": "revoked-rsa-dv.ssl.com",
            "Valid": true,
            "Reason": "352 days to expiry",
            "Time": "717.6289ms"
        }
    ]
}
```
